const express = require('express');

// imports all the rifles middleware functions
const {
  getRiflesRouteHandler,
  getRifleByNameRouteHandler,
  postRiflesRouteHandler,
  deleteRifleByNameRouteHandler,
  putRifleByNameRouteHandler,
} = require('../middleware/rifles.js');

// creates a router instance
const riflesRouter = express.Router();

// uses middleware to create a json object of the req body
riflesRouter.use(express.json());

// requests and endpoints helping map the appropriate middleware function or deeper routes
riflesRouter.get('/', getRiflesRouteHandler);
riflesRouter.get('/:name', getRifleByNameRouteHandler);
riflesRouter.post('/', postRiflesRouteHandler);
riflesRouter.put('/:name', putRifleByNameRouteHandler);
riflesRouter.delete('/:name', deleteRifleByNameRouteHandler);

// exports the rifle router
module.exports = riflesRouter;
