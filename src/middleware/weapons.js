const { getWeapons } = require('../models/weapons.js');

/**
 * Gets all the weapons(class) from the postgreDB and
 * sends the fetched data in response.
 */
function getWeaponsRouteHandler(req, res) {
  if (req.method !== 'GET') {
    res.send('This request is not allowed, only GET is allowed.');
  }
  getWeapons().then(weapons => {
    res.send(weapons);
  });
}
// exports all the weapons middleware functions
module.exports = {
  getWeaponsRouteHandler
};
